provider "kubernetes" {
  load_config_file       = "false"
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
    load_config_file       = false
  }
}

resource "kubernetes_namespace" "metrics" {
  depends_on = [
    module.eks
  ]
  metadata {
    name = "metrics"
  }
}


locals {
  googleapis="https://kubernetes-charts.storage.googleapis.com/"
  prometheus="https://prometheus-community.github.io/helm-charts"
  grafana="https://grafana.github.io/helm-charts"
}


data "helm_repository" "incubator" {
  name = "incubator"
  url  = "https://kubernetes-charts-incubator.storage.googleapis.com"
}


resource "helm_release" "goldpinger" {
  depends_on = [
    kubernetes_namespace.metrics
  ]
  name       = "goldpinger"
  repository = local.googleapis
  chart      = "goldpinger"
  version    = "2.0.2"
  namespace  = kubernetes_namespace.metrics.metadata[0].name
  values = [
    file("${path.module}/files/goldpinger-values.yaml")
  ]
}

resource "helm_release" "prometheus" {
  depends_on = [
    kubernetes_namespace.metrics
  ]
  name       = "prometheus"
  repository = local.prometheus
  chart      = "prometheus"
  namespace  = kubernetes_namespace.metrics.metadata[0].name
  values = [
    file("${path.module}/files/prometheus-values.yaml")
  ]
}

resource "helm_release" "grafana" {
  depends_on = [    
      helm_release.prometheus
  ]
  name       = "grafana"
  repository = local.grafana
  chart      = "grafana"
  namespace  = kubernetes_namespace.metrics.metadata[0].name
  values = [
    file("${path.module}/files/grafana-values.yaml")
  ]
}