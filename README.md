# Criação Cluste EKS Terraform  e aplicação latência entre Servidores usando Goldpinger
Etapas:  
Cria Cluster EKS (EKS, PVC, SUBNETES)  
Aplica Helm Goldpinger  
Aplica Helm Prometheus  
Aplica Helm Grafana  

Programas necessários:  
- Terraform  
- AWS cli  
- Kubectl  
- aws-iam-autenticator (para authenticar no cluster EKS)  

## Autenticando AWS
Pode expor as variáveis 
```
export AWS_ACCESS_KEY_ID=XXXXXXXXXXX
export AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXX
```

Ou criar arquivo   ~/.aws/credentials


# Rodando Terraform:
```
terraform init
terraform plan
terraform apply
```

# Acessando informações

Será criado arquivo kubeconfig.yaml para ter acesso ao cluster EKS  


# prometheus
kubectl port-forward --namespace metrics service/prometheus-server 8081:80


# Grafrana
kubectl port-forward -n metrics service/grafana 8080:80  
username: admin


##### pega senha acesso ao Grafana
kubectl get secret --namespace metrics grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

# Dashboard Grafana

Terá 2 dashboard um para EKS e outros para métricas Goldpinger.