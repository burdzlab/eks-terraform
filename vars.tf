variable "REGION" {
  default = "us-east-1"
}

variable "CLUSTER-NAME" {
  default  = "eks-terraform"
  type  = string
  description = "EKS Cluster Name"
}

variable "EKS-INSTANCE-DESIRED" {
  default     = 3
  type        = number
}

variable "EKS-INSTANCE-MAX" {
  default     = 3
  type        = number
}

variable "EKS-INSTANCE-MIN" {
  default     = 3
  type        = number
}