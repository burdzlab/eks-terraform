module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = var.CLUSTER-NAME
  cluster_version = "1.17"
  subnets         = module.vpc.private_subnets

  tags = {
    Environment = "Development"    
  }

  vpc_id = module.vpc.vpc_id

   # YOU CAN DECIDE USING NODEGROUP OR AUTOSCALING GROUP
  node_groups = {
    eks_t2small = {      
      instance_type = "t2.small"
      desired_capacity =  var.EKS-INSTANCE-DESIRED      
      max_capacity     =  var.EKS-INSTANCE-MAX
      min_capaicty     =  var.EKS-INSTANCE-MIN
    }
  }
  /*
  worker_groups = [
    {
      name = "small"
      instance_type = "t2.small" 
      asg_desired_capacity = var.EKS-INSTANCE-DESIRED
      asg_max_size  = var.EKS-INSTANCE-MAX
      asg_min_size = var.EKS-INSTANCE-MIN
    }
  ]
  */
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}